clear all
close all

dataset_list_paths = [...
                      %{'C:\Data\Code\general_code_old\data_folder_lists\Janelia\dataset_list_fore_distr_MBONAlpha1.xls'};...
                      %{'E:\Data\Adithya\Analysed_Data\datalists\alpha1_MBON_V1.xls'};...
                      %{'E:\Data\Adithya\Analysed_Data\datalists\alpha1_MBON_V2.xls'};...
                      {'E:\Data\Adithya\Analysed_Data\datalists\alpha1_MBON_V3.xls'};...
                      ];
            
suppress_plots = 0;
[del, odor_names1] = xlsread('E:\Documents\MATLAB\Olfactometer\DualOlfactometer\calibration\OdorList_AR_20190429.xls', 1);
[del, odor_names2] = xlsread('E:\Documents\MATLAB\Olfactometer\DualOlfactometer\calibration\odorList_olf2.xls', 1);


global color_vec;                
a = colormap('bone');
global greymap
greymap = flipud(a);
fly_n = 0;
script_name = mfilename;

%%

for list_n = 1:size(dataset_list_paths, 1)
    curr_dir_list_path = dataset_list_paths{list_n, 1};
    [del, dir_list] = xlsread(curr_dir_list_path, 1);        %list of Suite2P results directories
    n_dirs = size(dir_list, 1);
    dataset_list_name = findstr(curr_dir_list_path, 'list_');
    dataset_list_name = curr_dir_list_path((dataset_list_name + 5):(end - 4));
    
    saved_resps = zeros(n_dirs, 5) + nan;                    %mean response of each fly
    
    %loop to go through all experiment datasets listed in list file
    for dir_n = 1:n_dirs
        fly_n = fly_n + 1;
              
        saved_an_results.scriptname = mfilename('fullpath');
        curr_dir = [dir_list{dir_n, 1}, '\'];
        curr_dir = manage_base_paths_AR(curr_dir, 2);
       
        tif_times = load([curr_dir, 'tif_time_stamps.mat']);           %reading in time stamps for each tif file recorded by raw_data_extracter
        tif_times = tif_times.time_stamps;
        [stim_mat, stim_mat_simple, column_heads, color_vec] = load_params_trains_modular_AR(curr_dir, tif_times);    %reading in trial stimulus parameters after matching time stamps to F traces
        CSplus_colour = color_vec(1, :);
        CSminus_colour = color_vec(2, :);
        both_colour = color_vec(3, :);
        ctrl_colour = color_vec(4, :);
        
        odor_list_olf1 = unique(stim_mat_simple(:, 1) );
        n_odors_olf1 = length(odor_list_olf1);
        odor_dur_list_olf1 = unique(stim_mat_simple(:, 2) );
        n_od_durs_olf1 = length(odor_dur_list_olf1);
        n_trains_olf1 = max(stim_mat_simple(:, 8));
        
        odor_list_olf2 = unique(stim_mat_simple(:, 9) );
        odor_list_olf2 = odor_list_olf2(~isnan(odor_list_olf2));
        n_odors_olf2 = length(odor_list_olf2);
        odor_dur_list_olf2 = unique(stim_mat_simple(:, 10) );
        odor_dur_list_olf2 = odor_dur_list_olf2(~isnan(odor_dur_list_olf2));
        n_od_durs_olf2 = length(odor_dur_list_olf2);
        n_trains_olf2 = max(stim_mat_simple(:, 17));
        
        
        cd(curr_dir);
        tif_name = dir('*.tif');
        stack_obj = ScanImageTiffReader([curr_dir, tif_name(1).name]);
        [frame_time, zoom, n_chans, PMT_offsets] = SI_tif_info(stack_obj);
        
        %loading extracted raw fluorescence data matrices written by raw_dff_extractor
        raw_data_mat = load([curr_dir 'extracted_raw_data_mat.mat']);
        raw_data_mat = raw_data_mat.raw_data_mat;           %raw F traces extracted from ROIs
        raw_data_mat_orig = raw_data_mat;
        tif_n_col_n = find_stim_mat_simple_col('matched_tif_n', column_heads);
        raw_data_mat = raw_data_mat(:, :, stim_mat_simple(:, tif_n_col_n));       %making sure only time-stamp matched trials are used for further analysis
        n_cells = size(raw_data_mat, 2);
        
         %calculating dF/F traces from raw data
        filt_time = 0.5;            %in s, the time window for boxcar filter for generating filtered traces
        [dff_data_mat, dff_data_mat_f] = cal_dff_traces_res_AR(raw_data_mat, stim_mat, frame_time, filt_time, curr_dir);
        del = find(dff_data_mat_f < -1);
        dff_data_mat_f(del) = -1;       %forcing crazy values to sane ones
        
        %identifying stim_mat_simple col numbers
        led_on_col_n = find_stim_mat_simple_col('led_on', column_heads);            %identifying relevant column number in stim_mat_simple
        od_olf1_col_n = find_stim_mat_simple_col('odor_n', column_heads);           %identifying relevant column number in stim_mat_simple
        od_olf2_col_n = find_stim_mat_simple_col('odour_olf2', column_heads);       %identifying relevant column number in stim_mat_simple
        dur_olf1_col_n = find_stim_mat_simple_col('duration', column_heads);        %identifying relevant column number in stim_mat_simple
        dur_olf2_col_n = find_stim_mat_simple_col('duration_olf2', column_heads);   %identifying relevant column number in stim_mat_simple
        od_col_ns = [od_olf1_col_n, od_olf2_col_n];
        dur_col_ns = [dur_olf1_col_n, dur_olf2_col_n];
        
        both_tr_n = find(stim_mat_simple(:, od_olf2_col_n) == 4 & stim_mat_simple(:, dur_olf2_col_n) == 10 & stim_mat_simple(:, dur_olf1_col_n) == 10); % list of trials with CSminus immediately after CSplus
        CSplus_od_n_olf1 = stim_mat_simple(both_tr_n(1), od_olf1_col_n);          %This assumes the foreground odor is always delivered on olf1
        CSminus_od_n_olf2 = stim_mat_simple(both_tr_n(1), od_olf2_col_n);
        saved_resps(dir_n,5) = CSplus_od_n_olf1;                                 %keeping track of the foreground odor number for current dataset
        
        %identifying matching odor_ns for olf1 and olf2 based on odorNames
        %for the two olfactometers
        CSplus_od_name = odor_names1{CSplus_od_n_olf1};
        CSplus_od_n_olf2 = lookup_cell_vec(CSplus_od_name, odor_names2);
        CSminus_od_name = odor_names2{CSminus_od_n_olf2};
        CSminus_od_n_olf1 = lookup_cell_vec(CSminus_od_name, odor_names1);
        CSplus_od_n = CSplus_od_n_olf1;         %olf1 is always used to deliver the foreground odor
        CSminus_od_n = CSminus_od_n_olf2;        
        od_list_olf1 = unique(stim_mat_simple(:, od_olf1_col_n));                    %all odors delivered on olf1
        ctrl_od_ni = find(od_list_olf1 ~= CSplus_od_n_olf1 & od_list_olf1 ~= CSminus_od_n_olf1);
        ctrl_od_n_olf1 = od_list_olf1(ctrl_od_ni);
        
        %checking which olfactometer was used to deliver the foreground
        %odor in the pre-post trials.
        CSplus_trs_olf2 = find(stim_mat_simple(:, od_olf2_col_n) == CSplus_od_n_olf2);
        if isempty(CSplus_trs_olf2) ==  1
            CSplus_olf_n = 1;                 %olfactometer1 used for foreground odor delivery in pre-post trials
            CSminus_olf_n = 2;                %olfactometer2 used for distractor odor delivery in pre-post trials
            CSplus_od_col_n = od_olf1_col_n;
            CSplus_dur_col_n = dur_olf1_col_n;
            CSminus_od_col_n = od_olf2_col_n;
            CSminus_dur_col_n = dur_olf2_col_n;            
        else
            CSplus_olf_n = 2;                 %olfactometer2 used for foreground odor delivery in pre-post trials
            CSminus_olf_n = 1;                %olfactometer1 used for distractor odor delivery in pre-post trials
            CSplus_od_col_n = od_olf2_col_n;
            CSplus_dur_col_n = dur_olf2_col_n;
            CSminus_od_col_n = od_olf1_col_n;
            CSminus_dur_col_n = dur_olf1_col_n;
            
        end
        
         
        odor_dur = stim_mat_simple(both_tr_n(1), CSplus_dur_col_n);               
        odor_dur = max(odor_dur, [], 'omitnan');                        %duration that pre-post odors were delivered for
        
        
        CSplus_od_trs = find(stim_mat_simple(:, CSplus_od_col_n) == CSplus_od_n & stim_mat_simple(:, CSplus_dur_col_n) == odor_dur & stim_mat_simple(:, CSminus_dur_col_n) ~= odor_dur);             %list of CS plus odor presentation trials
        CSminus_od_trs = find(stim_mat_simple(:, CSminus_od_col_n) == CSminus_od_n & stim_mat_simple(:, CSminus_dur_col_n) == odor_dur & stim_mat_simple(:, CSplus_dur_col_n) ~= odor_dur);         %list of CS minus odor presentation trials
        both_od_trs = find(stim_mat_simple(:, CSminus_od_col_n) == CSminus_od_n & stim_mat_simple(:, CSminus_dur_col_n) == odor_dur & stim_mat_simple(:, CSplus_dur_col_n) == odor_dur); % list of trials with CSminus immediately after CSplus
        ctrl_od_trs = find(stim_mat_simple(:, od_olf1_col_n) == ctrl_od_n_olf1);
        
         %1. CSplus odor resps
        curr_trs = CSplus_od_trs;
        curr_trs = sort(curr_trs);
        curr_trs(1) = [];             %getting rid of first preesntation of foreground odour     
        curr_traces = dff_data_mat(:, :, curr_trs);
        stim_frs = compute_stim_frs_modular_AR(stim_mat, curr_trs(1), frame_time);
        stim_frs = stim_frs{CSplus_olf_n};
        mean_CSplus_trials = mean(squeeze(curr_traces(stim_frs(1):(stim_frs(2)), :, :)), 1, 'omitnan');
        saved_resps(dir_n, 1) = mean(mean(squeeze(curr_traces(stim_frs(1):(stim_frs(2)), :, :)), 1, 'omitnan'));
        trace_mean = mean(curr_traces, 3, 'omitnan');
        trace_se = std(curr_traces, [], 3, 'omitnan')./sqrt(length(curr_trs));
        
        figure(1)
%         plot(trace_mean, 'Color', fore_colour, 'lineWidth', 3)
%         hold on
%         plot(squeeze(curr_traces), 'Color', fore_colour, 'lineWidth', 0.2)
        shadedErrorBar([], trace_mean, trace_se, {'Color', CSplus_colour})
        hold on
%         for k = 1:length(curr_trs)
%             plot(squeeze(curr_traces(:,:,k)),'Color', CSplus_colour*0.1*k, 'linewidth', 0.2);
%         end
        ylabel('CSplus odor responses (dF/F)')
        set_xlabels_time(1, frame_time, 10);
        fig_wrapup(1, script_name);
        add_stim_bar(1, stim_frs, [0.75, 0.75, 0.75]);
        hold off   
%         
%         figure(5)
%         scatter([1:10],mean_CSplus_trials);
%         
%         
        %2. CSminus odor resps
        curr_trs = CSminus_od_trs;
        curr_trs = sort(curr_trs);
        curr_trs(1) = [];             %getting rid of first preesntation of foreground odour     
        curr_traces = dff_data_mat(:, :, curr_trs);
        stim_frs = compute_stim_frs_modular_AR(stim_mat, curr_trs(1), frame_time);
        stim_frs = stim_frs{CSminus_olf_n};
        saved_resps(dir_n, 2) = mean(mean(squeeze(curr_traces(stim_frs(1)+30:(stim_frs(2)), :, :)), 1, 'omitnan'));
        mean_CSminus_trials= mean(squeeze(curr_traces(stim_frs(1):(stim_frs(2)), :, :)), 1, 'omitnan');
        trace_mean = mean(curr_traces, 3, 'omitnan');
        trace_se = std(curr_traces, [], 3, 'omitnan')./sqrt(length(curr_trs));
        
        figure(2)
%         plot(trace_mean, 'Color', distr_colour, 'lineWidth', 3)
%         hold on
%         plot(squeeze(curr_traces), 'Color', distr_colour, 'lineWidth', 0.2)
        shadedErrorBar([], trace_mean, trace_se, {'Color', CSminus_colour})
        hold on
        
%         for k = 1:length(curr_trs)
%             plot(squeeze(curr_traces(:,:,k)),'Color', CSminus_colour*0.1*k, 'linewidth', 0.2);
%         end
        ylabel('CSminus odor responses (dF/F)')
        set_xlabels_time(2, frame_time, 10);
        fig_wrapup(2, script_name);
        add_stim_bar(2, stim_frs, [0.75, 0.75, 0.75]);
        hold off    
%         
%         figure(6)
%         scatter([1:10],mean_CSminus_trials);
%         
        %3. CSplus & CSminus odor resps
        curr_trs = both_od_trs;
        curr_trs = sort(curr_trs);
        curr_trs(1) = [];             %getting rid of first preesntation of foreground odour     
        curr_traces = dff_data_mat(:, :, curr_trs);
        stim_frs = compute_stim_frs_modular_AR(stim_mat, curr_trs(1), frame_time);
        stim_frs_1 = stim_frs{CSplus_olf_n}(1);
        stim_frs_2 = stim_frs{CSminus_olf_n}(2);
        stim_frs = [stim_frs_1,stim_frs_2];
        saved_resps(dir_n, 3) = mean(mean(squeeze(curr_traces(stim_frs(1)+50:(stim_frs(2)-150), :, :)), 1, 'omitnan'));
        mean_both_trials= mean(squeeze(curr_traces(stim_frs(1)+50:(stim_frs(2)-15 ), :, :)), 1, 'omitnan');
        trace_mean = mean(curr_traces, 3, 'omitnan');
        trace_se = std(curr_traces, [], 3, 'omitnan')./sqrt(length(curr_trs));
        
        figure(3)
%         plot(trace_mean, 'Color', fore_colour, 'lineWidth', 3)
%         hold on
%         plot(squeeze(curr_traces), 'Color', fore_colour, 'lineWidth', 0.2)
        shadedErrorBar([], trace_mean, trace_se, {'Color', both_colour})
        hold on
        
%         for k = 1:length(curr_trs)
%             plot(squeeze(curr_traces(:,:,k)),'Color', both_colour*0.1*k, 'linewidth', 0.2);
%         end
        ylabel('both odor responses (dF/F)')
        set_xlabels_time(3, frame_time, 10);
        fig_wrapup(3, script_name);
        add_stim_bar(3, stim_frs, [0.75, 0.75, 0.75]);
        hold off   
%         
%         figure(7)
%         scatter([1:10],mean_both_trials);       
%         
        %4 Ctrl odor pre responses
        curr_trs = ctrl_od_trs;
        curr_trs = sort(curr_trs);
        curr_trs(1) = [];  
        curr_traces = dff_data_mat(:, :, curr_trs);
        stim_frs = compute_stim_frs_modular_AR(stim_mat, curr_trs(1), frame_time);
        stim_frs = stim_frs{CSplus_olf_n};
        saved_resps(dir_n, 4) = mean(mean(squeeze(curr_traces(stim_frs(1):(stim_frs(2) ), :, :)), 1, 'omitnan'));
        mean_ctrl_trials= mean(squeeze(curr_traces(stim_frs(1):(stim_frs(2) ), :, :)), 1, 'omitnan');
        trace_mean = mean(curr_traces, 3, 'omitnan');
        trace_se = std(curr_traces, [], 3, 'omitnan')./sqrt(length(curr_trs));
        
        figure(4)
%         plot(trace_mean, 'Color', fore_colour, 'lineWidth', 3)
%         hold on
%         plot(squeeze(curr_traces), 'Color', fore_colour, 'lineWidth', 0.2)
        shadedErrorBar([], trace_mean, trace_se, {'Color', ctrl_colour})
        hold on
        
%         for k = 1:length(curr_trs)
%             plot(squeeze(curr_traces(:,:,k)),'Color', ctrl_colour*0.1*k, 'linewidth', 0.2);
% %             keyboard
%         end
        ylabel('ctrl odor responses (dF/F)')
        set_xlabels_time(4, frame_time, 10);
        fig_wrapup(4, script_name);
        add_stim_bar(4, stim_frs, [0.75, 0.75, 0.75]);
        hold off  
%         
%         figure(8)
%         scatter([1:10],mean_ctrl_trials);
%         
        figure(5)
        bar(transpose([mean_CSplus_trials;mean_CSminus_trials;mean_both_trials;mean_ctrl_trials]))


        if suppress_plots == 0
            keyboard
            
        else
        end
        close figure 1
        close figure 2
        close figure 3
        close figure 4
        close figure 5

            
    end
    marker_colors = [CSplus_colour; CSminus_colour; both_colour;  ctrl_colour;];
    scattered_dot_plot(saved_resps(:, 1:4), 6, 1, 4, 8, marker_colors, 1, [],[0.75, 0.75, 0.75],...
                            [{'CSplus'}, {'CSminus'},{'both'}, {'ctrl'}], 1, [0.35, 0.35, 0.35]);
    
    fig_wrapup(6, script_name);

end